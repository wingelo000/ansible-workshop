# Lab 1
## Objectif 

Configurer des groupes de serveurs Linux à l’aide d’un playbook 
Créer un fichier dans le répertoire playbooks appelé lab_1.yaml 

- Creer votre inventaire
- Configurez SELINUX en mode permissif sur tous vos hôtes
- Installez HTTPD et memcached seulement sur les hôtes du groupe Web
- Activez le service httpd sur les les hôtes du groupe Web
- Copiez un fichier motd indiquant « Bienvenue sur mon serveur! » sur tous vos hôtes (/etc/motd)
- Copiez un fichier index.html « Hello World » sur vos hôtes du groupe Web dans /var/www/html 
- Modifiez le sshd.conf pour mettre PermitRootLogin à no, et redémarrer le service ssh seulement si l’option est modifiée sur vos hôtes du groupe Web 
- Affichez le contenu de la variable ansible_distribution de tous vos hôtes
Commettez votre playbook 

### Modules utilisés dans ce laboratoire sont

| Module      | Documentation |
| ----------- | ------------- |
| selinux     | https://docs.ansible.com/ansible/latest/modules/selinux_module.html |
| yum         | https://docs.ansible.com/ansible/latest/modules/yum_module.html |
| service     | https://docs.ansible.com/ansible/latest/modules/service_module.html |
| copy        | https://docs.ansible.com/ansible/latest/modules/copy_module.html |
| lineinfile  | https://docs.ansible.com/ansible/latest/modules/lineinfile_module.html |
| debug       | https://docs.ansible.com/ansible/latest/modules/debug_module.html |

### Fichiers à copier sur les serveurs

| Fichier     | Description |
| ----------- | ----------- |
| index.html  | Page d'acceuil des serveurs Web |
| motd        | Page d'acceuil des connexions SSH |

